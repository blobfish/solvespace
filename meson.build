project('libsolvespace', 'cpp',
  version : '2.3.0',
  license : 'GPL-3+',
  meson_version : '>=0.49.999',
  default_options : ['warning_level=0', 'cpp_std=c++11']
) 

libsolvespace_sources = [
  'src/entity.cpp'
  , 'src/expr.cpp'
  , 'src/constraint.cpp'
  , 'src/constrainteq.cpp'
  , 'src/util.cpp'
  , 'src/lib.cpp'
  , 'src/system.cpp']

extra_args = []
if (host_machine.system() == 'windows')
  libsolvespace_sources += 'src/win32/w32util.cpp'
  extra_args += [
    '-D_CRT_SECURE_NO_DEPRECATE'
    , '-D_CRT_SECURE_NO_WARNINGS'
    , '-D_SCL_SECURE_NO_WARNINGS'
    , '-D_WIN32_WINNT=0x500'
    , '-D_WIN32_IE=_WIN32_WINNT'
    , '-DISOLATION_AWARE_ENABLED'
    , '-DWIN32'
    , '-DWIN32_LEAN_AND_MEAN'
    , '-DUNICODE'
    , '-DNOMINMAX'
    , '-D_UNICODE'
  ]
  if (meson.get_compiler('cpp').get_id() == 'msvc')
    extra_args += ['/wd5208']
  endif
else
  libsolvespace_sources += 'src/unix/unixutil.cpp'
endif

#can't stop the warnings coming through to cadseer build. Why?

libsolvespace = static_library('solvespace'
  , libsolvespace_sources
  , include_directories : include_directories(['include', 'src'], is_system : true)
  , cpp_args : ['-DLIBRARY', extra_args]
  , install : true)

if meson.is_subproject()
  libsolvespace_dep = declare_dependency(
    include_directories : include_directories(['include', 'src'], is_system : true)
    , link_with : libsolvespace)
else
  install_headers('include/slvs.h', subdir : 'libsolvespace')

  pkg = import('pkgconfig')
  pkg.generate(libsolvespace
    , description : 'solvespace solver library'
    , extra_cflags : 'cpp_std=c++11'
    , filebase : 'libsolvespace'
    , name : 'libsolvespace'
    , subdirs : 'libsolvespace')
endif
